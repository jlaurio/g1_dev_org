@isTest
private class Test_ContactAddress {

    static testMethod void TestContactAddress() { 
        
        Account acc = new Account();
        
        acc.Name = 'TEST ACCOUNT';
        acc.BillingStreet = '123 Test';
        acc.BillingPostalCode = '90200';
        acc.BillingCountry = 'USA';
        acc.BillingState = 'NY';
        acc.BillingCity = 'NY';
        
        insert acc;
        
        Contact con = new Contact();
        
        con.FirstName = 'TEST FIRST';
        con.LastName  = 'TEST LAST';
        con.Email     = 'test@test.com';
        con.AccountId = acc.Id;
        
        Contact con2 = new Contact();
        
        con2.FirstName = 'TEST FIRST2';
        con2.LastName  = 'TEST LAST2';
        con2.Email     = 'test2@test.com';
        con2.AccountId = acc.Id;
        
        List<Contact> insertCons = new List<Contact>();
        
        insertCons.add(con);
        insertCons.add(con2);
        
        insert insertCons;
        
        update acc;
    }
}
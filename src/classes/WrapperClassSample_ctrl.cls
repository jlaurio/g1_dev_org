public class WrapperClassSample_ctrl {
    public Wrapper[] wrapperArray {get;set;}
    public Wrapper[] wrapperSelectedsArray {get;set;}
    public Boolean hasSelected {get;set;}
    
    public WrapperClassSample_ctrl(){
        wrapperArray = new Wrapper[]{};
        wrapperSelectedsArray = new Wrapper[]{};
        for(Opportunity o: [Select Id, Name, OwnerId, AccountId, StageName, 
                                (Select Id, 
                                        OpportunityID,
                                        UnitPrice, 
                                        TotalPrice, 
                                        ServiceDate, 
                                        Quantity, 
                                        PricebookEntryId, 
                                        PricebookEntry.Product2Id, 
                                        PricebookEntry.Name, 
                                        ListPrice
                                    from OpportunityLineItems ) FROM Opportunity ORDER BY Name LIMIT 10]){
            Wrapper wrap = new Wrapper(o, o.OpportunityLineItems);
            wrapperArray.add(wrap);
        }
        hasSelected = false;
    }
    
    public PageReference selectOppty(){
        System.debug('***** wrapperArray : '+wrapperArray);
        
        wrapperSelectedsArray = new Wrapper[]{};
        for(Wrapper w:wrapperArray){
            if(w.isSelected){
                hasSelected = true;
                wrapperSelectedsArray.add(w);
            }
        }
        if(!hasSelected){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No Selected Items'));
        }
        return null;
    }
}
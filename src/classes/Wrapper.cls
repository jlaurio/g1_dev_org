public class Wrapper{
    public Opportunity oppty {get; set;}
    public List<OpportunityLineItem> opptyItems {get;set;}
    public Boolean isSelected {get;set;}
    public String sText {get;set;}
    
    public wrapper(Opportunity o){
        oppty = o;
        opptyItems = new List<OpportunityLineItem>();
        isSelected = false;
        sText = '';
    }
    
    public wrapper(Opportunity o, List<OpportunityLineItem> items){
        oppty = o;
        opptyItems = items;
        isSelected = false;
        sText = '';
    }
}
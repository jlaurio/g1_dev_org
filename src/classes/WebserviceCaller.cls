public class WebserviceCaller{
    public string sessionId{get;set;}//value to be given
    
    public void callCreateAccount(){
        soapWebserviceSample.WebserviceSample soap = new soapWebserviceSample.WebserviceSample();
        
        soapWebserviceSample.SessionHeader_element sessionHeader = new soapWebserviceSample.SessionHeader_element();
        sessionHeader.sessionId = sessionId;
        
        soapWebserviceSample.InputWrapper input = new soapWebserviceSample.InputWrapper();
        input.AccountNumber = '12345';
        input.Name = 'Webservice Sample';
        
        soap.SessionHeader = sessionHeader;
        String response = soap.createAccount(input);
        
        System.debug('***** '+response );
    }
}
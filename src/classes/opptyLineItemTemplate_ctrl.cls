public class opptyLineItemTemplate_ctrl{
    public Id thisOpptyID {get;set;}
    public List<LineItemsWrapper> thisLineItems = new List<LineItemsWrapper>();
    
    public List<LineItemsWrapper> getThisLineItems(){
        System.debug('*****'+thisOpptyID);
        for(OpportunityLineItem oli: [SELECT 
                                        Quantity, OpportunityId, Description, ServiceDate, 
                                        Id, ListPrice, PricebookEntryId, UnitPrice, TotalPrice, 
                                        PricebookEntry.name 
                                    FROM OpportunityLineItem
                                    WHERE OpportunityId =: thisOpptyID]){
            LineItemsWrapper liw = new LineItemsWrapper(oli);
            thisLineItems.add(liw);
        }
        
        return thisLineItems;
    }
    public class LineItemsWrapper{
        public OpportunityLineItem lineItem{get;set;}
        public String sAction{get;set;}
        public String scomment{get;set;}
        
        public LineItemsWrapper(OpportunityLineItem oli){
            lineItem = oli;
            sAction='';
            sComment='test value:'+oli.Description;
        }
    }
}
trigger AcctChildUpdate on Account (after update) {
    Set<Id> accIds = new Set<Id>();
    for(Account a :trigger.new){
        accIds.add(a.Id);
    }
    List<Contact> accContacts = new List<Contact>();
    accContacts = [Select Id, AccountId, MailingCity, MailingCountry, MailingState, MailingPostalCode, MailingStreet
                   From   Contact
                   Where  AccountId IN :accIds
                  ];
    Map<Id, List<Contact>> contactMap = new Map<Id, List<Contact>>();
    for(Contact c :accContacts){
        if(contactMap.containsKey(c.AccountId)){
            contactMap.get(c.AccountId).add(c);
        }
        else{
            List<Contact> conList = new List<Contact>();
            conList.add(c);
            contactMap.put(c.AccountId, conList);
        }
    }
    system.debug('@@ ' + contactMap);
    List<Contact> updateContactList = new List<Contact>();
    
    for(Account a :trigger.new){
    
        if(contactMap.get(a.Id) != null){
            for(Contact c :contactMap.get(a.Id)){
                c.MailingCity       = a.BillingCity;
                c.MailingCountry    = a.BillingCountry;
                c.MailingState      = a.BillingState;
                c.MailingPostalCode = a.BillingPostalCode;
                c.MailingCountry    = a.BillingCountry;
                c.MailingStreet     = a.BillingStreet;
                updateContactList.add(c);
            }
        }
    }
    
    if(updateContactList.size() > 0){
        system.debug('@@ ' + updateContactList);
        update updateContactList;
    }
}
trigger checkAccountChildbeforeDelete on Account(before delete){
    for(Contact cItems: [Select Id, AccountId from Contact where AccountId in: trigger.oldMap.keySet()]){
        Trigger.oldMap.get(cItems.AccountId).Id.addError('Cannot delete Account with a contact');
    }
}
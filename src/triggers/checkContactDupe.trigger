trigger checkContactDupe on Contact (before insert, before update) {
    List<String> conEmails = new List<String>();
    List<String> conName   = new List<String>();
    
    Set<Id> conIds = new Set<Id>();
    
    for(Contact c :trigger.new){
        conIds.add(c.Id);
        conName.add(c.FirstName + c.LastName);
        if(c.Email != null){
            conEmails.add(c.Email);
        }
    }
    
    List<Contact> existingContacts = new List<Contact>();
    Set<String> emailSet = new Set<String>();
    Set<String> nameSet  = new Set<String>();
    
    existingContacts = [Select FirstName, LastName, Email From Contact Where (Name IN :conName Or Email IN:conEmails) And Id Not In :conIds];
    
    for(Contact c :existingContacts){
        emailSet.add(c.Email);
        nameSet.add(c.FirstName + c.LastName);
    }

    for(Contact c : trigger.new){
        if(nameSet.contains(c.FirstName + c.LastName)){
            c.LastName.addError('A contact with this name already exists.');
        }
        if(c.Email != null){
            if(emailSet.contains(c.Email)){                            
                c.Email.addError('A contact with this email already exists.');            
            }
        }
    }    
}